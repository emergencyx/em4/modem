﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.PluginBase.Interface
{
	public interface ICommand
	{
		string Name
		{
			get;
		}

		string Description
		{
			get;
		}

		int Execute(string eventName);
	}
}
