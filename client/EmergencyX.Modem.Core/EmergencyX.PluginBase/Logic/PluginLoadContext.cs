﻿using EmergencyX.PluginBase.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.PluginBase.Logic
{
	public class PluginLoadContext
	{
		public static List<ICommand> Plugins
		{
			get;
			set;
		}

		public static void LoadPlugin(string[] pluginDirs)
		{
			PluginLoadContext.Plugins = new List<ICommand>();

			foreach (var item in pluginDirs)
			{
				if(!Directory.Exists(item))
				{
					continue;
				}

				foreach (var file in Directory.EnumerateFiles(item))
				{
					if(file.EndsWith(".dll"))
					{
						Assembly.LoadFile(Path.GetFullPath(file));
					}
				}
			}

			Type type = typeof(ICommand);
			Type[] types = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(x => x.GetTypes())
				.Where(t => type.IsAssignableFrom(t) && t.IsClass)
				.ToArray();

			foreach (var item in types)
			{
				PluginLoadContext.Plugins.Add((ICommand)Activator.CreateInstance(item));
			}

		}
	}
}
