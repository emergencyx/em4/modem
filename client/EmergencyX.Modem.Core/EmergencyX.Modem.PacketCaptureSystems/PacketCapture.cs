using System;
using PacketDotNet;
using SharpPcap;
using SharpPcap.LibPcap;
using SharpPcap.WinPcap;

namespace EmergencyX_Mapper
{
	public class PacketCapture
	{
		private const int ReadTimeOut = 1000;
		private static CaptureFileWriterDevice _packetCapturer;

		public static CaptureFileWriterDevice PacketCapturer
		{
			get { return _packetCapturer; }
			set { _packetCapturer = value; }
		}


		public static void ConfigureAndRun()
		{
			var deviceList = LibPcapLiveDeviceList.Instance;

			if (deviceList.Count < 1)
			{
				Console.WriteLine("Keine Geräte zum capturen");
				return;
			}

			Console.WriteLine("Geräte erkannt:");
			int selected = 0;
			foreach (var device in deviceList)
			{
				Console.WriteLine($"{selected}: {device.Name} -  {device.Description}");
				selected++;
			}

			Console.WriteLine("Gerät wählen:");
			selected = int.Parse(Console.ReadLine());
			Console.WriteLine("Output-Pfad");
			var path = Console.ReadLine();

			var selectedDevice = deviceList[selected];

			selectedDevice.OnPacketArrival += OnPacketCapture;

			if (selectedDevice is WinPcapDevice)
			{
				var npcap = selectedDevice as WinPcapDevice;
				npcap.Open(SharpPcap.WinPcap.OpenFlags.DataTransferUdp | SharpPcap.WinPcap.OpenFlags.NoCaptureLocal, ReadTimeOut);
			}
			else if (selectedDevice is LibPcapLiveDevice)
			{
				var livePcapDevice = selectedDevice as LibPcapLiveDevice;
				livePcapDevice.Open(DeviceMode.Promiscuous, ReadTimeOut);
			}

			Console.WriteLine();
			Console.WriteLine($"Begin listening on device {selectedDevice.Name} writing to {path}. Any Key for end");

			PacketCapturer = new CaptureFileWriterDevice(selectedDevice, path);
			selectedDevice.Filter = "port 40000";
			selectedDevice.StartCapture();

			Console.ReadLine();

			selectedDevice.StopCapture();
			Console.WriteLine("Capturing has stopped");
			selectedDevice.Close();
			Console.ReadLine();

		}

		private static void OnPacketCapture(object sender, CaptureEventArgs e)
		{
			PacketCapturer.Write(e.Packet);
			Console.WriteLine("Packet to file writen");
		}
	}
}