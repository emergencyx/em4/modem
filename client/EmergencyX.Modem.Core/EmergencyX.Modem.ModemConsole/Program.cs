﻿using EmergencyX.PluginBase.Interface;
using EmergencyX.PluginBase.Logic;
using EM4Modem = EmergencyX.Modem.Core.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using EmergencyX_Mapper;

namespace EmergencyX.Modem.ModemConsole
{
	class Program
	{
		static string[] _pluginsPath = new string[]
		{
			"./plugins"
		};

		const bool CAPTURESWITCH = false;
		
		static void Main(string[] args)
		{
			try
			{
				PluginLoadContext.LoadPlugin(_pluginsPath);
			}
			catch (Exception)
			{
				Console.WriteLine("Failed to load plugins");
			}

			// Create modem and run it
			EM4Modem.Modem.CreateModem("mars.emergencyx.de", 40000, CAPTURESWITCH);

			try
			{
				EM4Modem.Modem.PacketRouter.Run();
			}
			catch (Exception e)
			{

				Console.WriteLine(e.Message);
				Console.WriteLine(e.StackTrace);
				Console.WriteLine(e.InnerException);
				throw;
			}

			if(CAPTURESWITCH)
			{
				// Capture packets
				PacketCapture.ConfigureAndRun();
			}
			
			// Wait
			Console.ReadKey();

			// End
			EM4Modem.Modem.PacketRouter.Stop();
			Console.WriteLine("Modem stopped. Press any key to end.");
			Console.ReadKey();
		}

		static void CommandRunner(string eventName)
		{
			if (PluginLoadContext.Plugins == null || PluginLoadContext.Plugins.Count == 0)
			{
				return;
			}
			
			foreach (var item in PluginLoadContext.Plugins)
			{
				item.Execute(eventName);
			}
		}
	}
}