﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.Modem.Core.Utility
{
	class DataUtil
	{
		/// <summary>
		/// Creates a Hexstring from an byte array
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		internal static string GetFormattedHexLogEntry(byte[] data)
		{
			var hexData = String.Concat(Array.ConvertAll(data, x => x.ToString("X2")));
			var logEntry = String.Format("{0};{1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss,fff"), hexData);
			return logEntry;
		}
	}
}
