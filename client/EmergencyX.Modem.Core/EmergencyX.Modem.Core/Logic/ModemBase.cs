﻿namespace EmergencyX.Modem.Core.Logic
{
	public abstract class ModemBase
	{
		private string _domain;
		private int _port;

		internal int Port
		{
			get { return _port; }
			set { _port = value; }
		}

		internal string Domain
		{
			get { return _domain; }
			set { _domain = value; }
		}
	}
}