﻿using EmergencyX.Modem.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmergencyX.Modem.Core.Logic
{
	internal class PlayerGameConnection : ModemBase
	{
		private const int BUFFERSIZE = 1024;
		private TcpClient _gameListener;
		private NetworkStream _gameStream;
		private byte[] _gameReceiveFromBuffer = new byte[BUFFERSIZE];
		private string _playerId;
		private CancellationTokenSource cancelRead;
		private int _assignedEndpointPort;

		internal TcpClient GameListener
		{
			get { return _gameListener; }
			set { _gameListener = value; }
		}

		internal byte[] GameReceiveFromBuffer
		{
			get { return _gameReceiveFromBuffer; }
			set { _gameReceiveFromBuffer = value; }
		}

		internal NetworkStream GameStream
		{
			get { return _gameStream; }
			set { _gameStream = value; }
		}

		public string PlayerId
		{
			get
			{
				return _playerId;
			}

			set
			{
				_playerId = value;
			}
		}

		internal CancellationTokenSource CancelRead
		{
			get
			{
				return cancelRead;
			}

			set
			{
				cancelRead = value;
			}
		}

		internal int AssignedEndpointPort
		{
			get
			{
				return _assignedEndpointPort;
			}

			set
			{
				_assignedEndpointPort = value;
			}
		}

		internal PlayerGameConnection(string domain, int port, string player, int assignedPort)
		{
			this.Domain = domain;
			this.Port = port;
			this.CancelRead = new CancellationTokenSource();
			this.PlayerId = player;
		}

		internal void RunPlayerGameConnection()
		{
			// Create the TCPClient with a IPEndpoint so we can assign different source ports
			IPAddress ip = IPAddress.Parse("127.0.0.1");
			IPEndPoint localEndpoint = new IPEndPoint(ip, AssignedEndpointPort);
			this.GameListener = new TcpClient(localEndpoint);

			// connect to the game
			this.GameListener.Connect(Domain, Port);
			this.GameStream = this.GameListener.GetStream();
			Console.WriteLine("Client for {0} connected", PlayerId);
			this.ReadGameAsync(GameReceiveFromBuffer);
		}

		/// <summary>
		/// Reads incoming data from Emergency 4 also asyncronus
		/// </summary>
		/// <param name="gameReceiveFromBuffer"></param>
		private async void ReadGameAsync(byte[] gameReceiveFromBuffer)
		{
			try
			{
				var result = await this.GameStream.ReadAsync(gameReceiveFromBuffer, 0, gameReceiveFromBuffer.Length, this.CancelRead.Token);
				this.OnReadGame(gameReceiveFromBuffer, result);
			}
			catch (Exception e)
			{

				Console.WriteLine(e.Message);
				this.Disconnect(true);
			}
		}

		/// <summary>
		/// Handles incoming data from the game
		/// </summary>
		/// <param name="data"></param>
		/// <param name="taskResult"></param>
		private void OnReadGame(byte[] data, int taskResult)
		{
			// only continue if actual data was received
			if (taskResult == 0)
			{
				return;
			}

			// only take the actual data
			var dataToSent = data.Take(taskResult).ToArray();

			// EM4 Multiplayerprotocol
			//if (CAPTURE)
			//{
			//	var captureArray = Encoding.UTF8.GetBytes(DataUtil.GetFormattedHexLogEntry(dataToSent));
			//	this.CaptureToServer.WriteAsync(captureArray, 0, captureArray.Length);
			//	this.CaptureToServerRawBytes.WriteAsync(dataToSent, 0, dataToSent.Length);
			//}

			// send data to modem server
			if (Modem.PacketRouter != null)
			{
				var message = new Packet() { Connection = this.PlayerId, Payload = dataToSent };
				Modem.PacketRouter.SendToModemServerAsync(message);
			}

			Array.Clear(data, 0, data.Length);

			// wait for data
			this.ReadGameAsync(data);
		}

		internal void Disconnect(bool isEM4Close)
		{
			if (isEM4Close)
			{
				var message = new Close() { Connection = this.PlayerId };
				Modem.PacketRouter.SendToModemServerAsync(message);
			}

			this.GameStream.Close();
			this.GameListener.Close();
		}
	}
}
