using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Serialization;

namespace EmergencyX.Modem.Core.Logic
{
	public class AllowedTypesBinder : ISerializationBinder
	{
		private List<Type> _allowedTypes;

		public List<Type> AllowedTypes
		{
			get { return _allowedTypes; }
			set { _allowedTypes = value; }
		}

		public Type BindToType(string assemblyName, string typeName)
		{
			return AllowedTypes.SingleOrDefault(x => x.Name == typeName);
		}

		public void BindToName(Type serializedType, out string assemblyName, out string typeName)
		{
			assemblyName = null;
			typeName = serializedType.Name;
		}
	}
}