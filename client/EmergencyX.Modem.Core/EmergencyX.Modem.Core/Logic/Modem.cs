using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using EmergencyX.Modem.Core.Data;
using Newtonsoft.Json;
using EmergencyX.Modem.Core.Utility;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace EmergencyX.Modem.Core.Logic
{
	public class Modem : ModemBase
	{
		private static Modem _packetRouter;

		#region Member
		private readonly bool CAPTURE;
		private AllowedTypesBinder _allowed = new AllowedTypesBinder() { AllowedTypes = new List<Type>() { typeof(Welcome), typeof(Packet), typeof(Close), typeof(Error), typeof(PlayerConnected), typeof(PlayerDisconnected) } };
		private readonly List<int> _endpointPorts;
		private ConcurrentQueue<IModemMessage> _messageQueue;
		private Task _messageSender;

		private ClientWebSocket _modemServerListener;
		private ArraySegment<byte> _serverReceiveFromBuffer;
		private ArraySegment<byte> _serverSendToBuffer;

		private List<PlayerGameConnection> _players;

		private FileStream _captureToServer;
		private FileStream _captureToGame;
		private FileStream _captureToServerRawBytes;
		private FileStream _captureToGameRawBytes;
		#endregion

		#region Propertys

		internal ClientWebSocket ModemServerListener
		{
			get { return _modemServerListener; }
			set { _modemServerListener = value; }
		}

		internal ArraySegment<byte> ServerReceiveFromBuffer
		{
			get { return _serverReceiveFromBuffer; }
			set { _serverReceiveFromBuffer = value; }
		}

		internal FileStream CaptureToServer
		{
			get { return _captureToServer; }
			set { _captureToServer = value; }
		}

		internal FileStream CaptureToGame
		{
			get { return _captureToGame; }
			set { _captureToGame = value; }
		}

		internal FileStream CaptureToServerRawBytes
		{
			get { return _captureToServerRawBytes; }
			set { _captureToServerRawBytes = value; }
		}

		internal FileStream CaptureToGameRawBytes
		{
			get { return _captureToGameRawBytes; }
			set { _captureToGameRawBytes = value; }
		}

		internal JsonSerializerSettings GetJsonSerializerSettings
		{
			get
			{
				return new JsonSerializerSettings()
				{ TypeNameHandling = TypeNameHandling.All, SerializationBinder = _allowed };
			}
		}

		internal ArraySegment<byte> GetNewArraySegment
		{
			get
			{
				return WebSocket.CreateClientBuffer(1024, 1024);
			}
		}

		internal List<PlayerGameConnection> Players
		{
			get
			{
				return _players;
			}

			set
			{
				_players = value;
			}
		}

		public static Modem PacketRouter
		{
			get
			{
				return _packetRouter;
			}

			internal set
			{
				_packetRouter = value;
			}
		}

		public ArraySegment<byte> ServerSendToBuffer
		{
			get
			{
				return _serverSendToBuffer;
			}

			set
			{
				_serverSendToBuffer = value;
			}
		}

		internal List<int> EndpointPorts
		{
			get
			{
				return _endpointPorts;
			}
		}

		internal ConcurrentQueue<IModemMessage> MessageQueue
		{
			get
			{
				return _messageQueue;
			}

			set
			{
				_messageQueue = value;
			}
		}

		public Task MessageSender
		{
			get
			{
				return _messageSender;
			}

			set
			{
				_messageSender = value;
			}
		}

		#endregion

		#region ExposedMethods
		public static void CreateModem(string domain, int port, bool capture)
		{
			Modem.PacketRouter = new Modem(domain, port, capture);
		}

		/// <summary>
		/// Runs the created modem asyncronus
		/// </summary>
		public async void Run()
		{
			this.ModemServerListener = new ClientWebSocket();
			await this.ModemServerListener.ConnectAsync(new Uri("ws://mars.emergencyx.de:40000"), CancellationToken.None);
			Console.WriteLine("Running modem");
			this.MessageSender = Task.Factory.StartNew(() => SendMessage(), TaskCreationOptions.LongRunning);
			this.ReadModemServerAsync(ServerReceiveFromBuffer);
		}

		/// <summary>
		/// Stops a running modem
		/// </summary>
		public void Stop()
		{
			this.CaptureToGame.Close();
			this.CaptureToServer.Close();
			this.CaptureToGameRawBytes.Close();
			this.CaptureToServerRawBytes.Close();
			this.ModemServerListener.CloseAsync(WebSocketCloseStatus.NormalClosure, "End of connection", CancellationToken.None);
		}

		#endregion

		#region PrivateConstructor
		/// <summary>
		/// Consturctor for Modem
		/// </summary>
		/// <param name="domain"></param>
		/// <param name="port"></param>
		/// <param name="capture"></param>
		private Modem(string domain, int port, bool capture)
		{
			this.Domain = domain;
			this.Port = port;
			this.CAPTURE = capture;
			this.ServerReceiveFromBuffer = GetNewArraySegment;
			this.ServerSendToBuffer = GetNewArraySegment;

			this._endpointPorts = new List<int>() { 50000, 50001, 50002 };
			this.MessageQueue = new ConcurrentQueue<IModemMessage>();

			#region CaptureFiles
			this.CaptureToGame = File.Create(@"C:\temp\EM4ToGameStream.log");
			this.CaptureToServer = File.Create(@"C:\temp\EM4ToServer.log");
			this.CaptureToGameRawBytes = File.Create(@"C:\temp\EM4ToGameRawStream.log");
			this.CaptureToServerRawBytes = File.Create(@"C:\temp\EM4ToServerRawStream.log");
			#endregion

			JsonSerializerSettings settings = new JsonSerializerSettings()
			{ TypeNameHandling = TypeNameHandling.All };

			Console.WriteLine("Modem adapter ready");
		}
		#endregion

		#region ModemServerRelated
		/// <summary>
		/// Reads the WebSocket in Async mode
		/// </summary>
		/// <param name="serverReceiveFromBuffer"></param>
		private async void ReadModemServerAsync(ArraySegment<byte> serverReceiveFromBuffer)
		{
			WebSocketReceiveResult result;
			using (MemoryStream stream = new MemoryStream())
			{
				// use loop to receive complete message and not only chunks
				do
				{
					result = await this.ModemServerListener.ReceiveAsync(serverReceiveFromBuffer, CancellationToken.None);
					stream.Write(serverReceiveFromBuffer.Array, serverReceiveFromBuffer.Offset, result.Count);

				} while (result.EndOfMessage == false);

				stream.Seek(0, SeekOrigin.Begin);

				this.OnReadModemServer(stream.ToArray(), serverReceiveFromBuffer, result);
			}
		}

		private async Task SendMessage()
		{
			while (true)
			{
				IModemMessage message;
				if(this.MessageQueue.TryDequeue(out message))
				{
					var str = JsonConvert.SerializeObject(message, GetJsonSerializerSettings);
					var data = Encoding.UTF8.GetBytes(str);
					this.ServerSendToBuffer = new ArraySegment<byte>(data, 0, data.Length);
					await this.ModemServerListener.SendAsync(ServerSendToBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
				}
			}
		}

		internal void SendToModemServerAsync(IModemMessage message)
		{
			this.MessageQueue.Enqueue(message);
		}

		/// <summary>
		/// Handles incoming data from the server through the primary Websocket
		/// </summary>
		/// <param name="data"></param>
		/// <param name="buffer"></param>
		/// <param name="taskResult"></param>
		private void OnReadModemServer(byte[] data, ArraySegment<byte> buffer, WebSocketReceiveResult taskResult)
		{
			// if no data was sent, skip
			if (taskResult.Count == 0)
			{
				return;
			}

			// get message type from message
			var messageObj = this.DecodeIncomingData(data);
			if (messageObj.GetType() == typeof(Welcome))
			{
				this.OnWelcomeMessageReceived(messageObj as Welcome);
			}
			else if (messageObj.GetType() == typeof(Packet))
			{
				// only use actual data
				this.OnPacketMessageReceived(messageObj as Packet);
			}
			else if (messageObj.GetType() == typeof(Error))
			{
				this.OnErrorMessageReceived(messageObj as Error);
			}
			else if (messageObj.GetType() == typeof(PlayerConnected))
			{
				this.OnPlayerConnectedMessageReceived(messageObj as PlayerConnected);
			}
			else if (messageObj.GetType() == typeof(PlayerDisconnected))
			{
				this.OnPlayerDisconnectedMessageReceived(messageObj as PlayerDisconnected);
			}
			else
			{
				throw new InvalidOperationException("Invalid operation at incoming data from WebSocket");
			}

			// clear buffer and wait again for data
			buffer = GetNewArraySegment;
			this.ReadModemServerAsync(buffer);
			return;

			//// EM 4 Multiplayerprotocol
			//if (CAPTURE)
			//{
			//	var captureArray = Encoding.UTF8.GetBytes(DataUtil.GetFormattedHexLogEntry(dataToSent));
			//	this.CaptureToGame.WriteAsync(captureArray, 0, captureArray.Length);
			//	this.CaptureToGameRawBytes.WriteAsync(dataToSent, 0, dataToSent.Length);
			//}
		}

		private void OnPacketMessageReceived(Packet data)
		{
			if(this.Players == null)
			{
				throw new InvalidOperationException();
			}

			var player = this.Players.SingleOrDefault(x => x.PlayerId == data.Connection);

			// check if this is the first data that is sent to the game for this player
			if(player.GameListener == null)
			{
				player.RunPlayerGameConnection();
			}

			player.GameStream.WriteAsync(data.Payload, 0, data.Payload.Length);
		}

		/// <summary>
		/// Handles Welcome messages from the server through the primary Websocket
		/// </summary>
		/// <param name="welcome"></param>
		private void OnWelcomeMessageReceived(Welcome welcome)
		{
			// when data first time is sent, establish connection to the game
			Console.WriteLine("Welcome message received, port is {0}", welcome.Port);
		}
		#endregion

		#region JoiningPlayersRelated
		/// <summary>
		/// Disconnects a TCPClient from the local EM4
		/// </summary>
		/// <param name="playerDisconnected"></param>
		private void OnPlayerDisconnectedMessageReceived(PlayerDisconnected playerDisconnected)
		{
			if (this.Players == null)
			{
				throw new InvalidOperationException();
			}

			if (this.Players.Count(x => x.PlayerId == playerDisconnected.Connection) == 1)
			{
				this.Players.SingleOrDefault(x => x.PlayerId == playerDisconnected.Connection)?.Disconnect(false);
				this.Players.RemoveAll(x => x.PlayerId == playerDisconnected.Connection);
			}
			else
			{
				throw new InvalidOperationException();
			}

			Console.WriteLine("Player '{0}' disconnected", playerDisconnected.Connection);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="playerConnected"></param>
		private void OnPlayerConnectedMessageReceived(PlayerConnected playerConnected)
		{
			if(this.Players == null)
			{
				this.Players = new List<PlayerGameConnection>();
			}

			if (this.Players.Count(x => x.PlayerId == playerConnected.Connection) == 0)
			{
				var item = new PlayerGameConnection("localhost", 58282, playerConnected.Connection, this.EndpointPorts[this.Players.Count]);
				this.Players.Add(item);
			}
			else
			{
				throw new InvalidOperationException();
			}
			Console.WriteLine("Player joined");
		}
		#endregion

		#region Helpers
		/// <summary>
		/// Creates a json string from incoming bytes and converts it to an IModemMessage
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private IModemMessage DecodeIncomingData(byte[] data)
		{
			var message = Encoding.UTF8.GetString(data);
			IModemMessage messageObj = JsonConvert.DeserializeObject<IModemMessage>(message, GetJsonSerializerSettings);
			return messageObj;
		}

		/// <summary>
		/// Error Handler for error messages from Server
		/// </summary>
		/// <param name="error"></param>
		private void OnErrorMessageReceived(Error error)
		{
			throw new Exception(String.Format("Error during server communication. Error is: {0}", (error as Error).Reason));
		}
		#endregion
	}
}