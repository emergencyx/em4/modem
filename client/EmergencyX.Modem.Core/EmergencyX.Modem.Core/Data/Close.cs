﻿using Newtonsoft.Json;

namespace EmergencyX.Modem.Core.Data
{
	public class Close : IModemMessage
	{
		private string _connection;

		[JsonProperty("connection")]
		public string Connection
		{
			get
			{
				return _connection;
			}

			set
			{
				_connection = value;
			}
		}
	}
}
