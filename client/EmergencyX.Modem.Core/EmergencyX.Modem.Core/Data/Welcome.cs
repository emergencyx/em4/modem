using Newtonsoft.Json;

namespace EmergencyX.Modem.Core.Data
{
	public class Welcome : IModemMessage
	{
		private ushort _port;

		[JsonProperty("port")]
		public ushort Port
		{
			get { return _port; }
			set { _port = value; }
		}
	}
}