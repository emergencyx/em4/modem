using Newtonsoft.Json;

namespace EmergencyX.Modem.Core.Data
{
	public class Packet : IModemMessage
	{
		private string _connection;
		private byte[] _payload;

		[JsonProperty("payload")]
		public byte[] Payload
		{
			get { return _payload; }
			set { _payload = value; }
		}

		[JsonProperty("connection")]
		public string Connection
		{
			get { return _connection; }
			set { _connection = value; }
		}
	}
}