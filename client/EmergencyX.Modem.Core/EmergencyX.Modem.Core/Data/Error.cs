﻿using Newtonsoft.Json;

namespace EmergencyX.Modem.Core.Data
{
	public class Error : IModemMessage
	{
		private string _reason;

		[JsonProperty("reason")]
		public string Reason
		{
			get
			{
				return _reason;
			}

			set
			{
				_reason = value;
			}
		}
	}
}
