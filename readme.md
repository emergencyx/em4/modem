# em4-modem
Emergency 4 Multiplayer mit mehr als vier Spielern und über IPv6

## Ziele
Nach Wichtigkeit in absteigender Reihenfolge geplant:
- Mehrspieler mit mehr als vier Personen gleichzeitig (Multiplexing)
- Client sollte keine Änderungen erfordern im Idealfall
- Kompatibilität zu IPv6

## Client-Kompatibilität
Emergency 4 fragt den Masterserver per UDP auf Port 54321 nach allen verfügbaren Servern. Danach probiert der Client selbstständig jeden Server per UDP auf Port 12345 anzusprechen und dort eine Serverliste zu empfangen. Er erwartet in beiden Fällen die Antwort zurück auf Port 12345, nutzt diesen aber auch für die Anfragen, von daher sollten wir das Mapping einer Dual Stack NAT in diesem Fall erkennen können.
In der Abfrage ist eine Server IP und ein Server Port vorhanden, dort verbindet sich der Client aber von sich ausgehend.

Im Notfall leiten wir allen Verkehr über EMX Server um, der auf jeden Fall eine öffentliche IPv4 behalten wird.