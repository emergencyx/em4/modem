require('dotenv').config();

const uuid = require('uuid').v4;
const net = require('net');
const WebSocket = require('ws');

const ports = [50001, 50002, 50003, 50004, 50005, 50006, 58282];

if (process.env.SENTRY_DSN) {
    const Sentry = require('@sentry/node');
    Sentry.init({ dsn: process.env.SENTRY_DSN });
}

const server = new WebSocket.Server({ port: process.env.APP_PORT });
server.on('connection', (ws) => {
    // assign new port to listen on
    const port = ports.pop();
    if (!port) {
        console.log(`Rejecting client - ports exhausted`);
        const reason = JSON.stringify({ $type: 'Error', reason: 'Ports exhausted' });
        ws.send(reason); // send this twice
        ws.close(1013, reason);
        return;
    }

    ws.id = uuid();
    ws.players = new Set();
    ws.endpoint = net.createServer(player => {
        player.id = uuid();
        console.log(`New connection from player ${player.id}`);
        ws.players.add(player);

        ws.send(JSON.stringify({
            $type: 'PlayerConnected',
            connection: player.id
        }));

        player.once('close', () => {
            console.log(`Player ${player.id} disconnected`);
            ws.players.delete(player);
            ws.send(JSON.stringify({
                $type: 'PlayerDisconnected',
                connection: player.id
            }));
        });

        player.once('error', (e) => {
            console.log(`Player ${player.id} errored out`, e);
            player.destroy(e);
        });

        player.on('data', (data) => {
            console.log(`Player ${player.id} sent ${data.toString('base64')}`);
            // forward to client
            ws.send(JSON.stringify({
                $type: 'Packet',
                connection: player.id,
                payload: data.toString('base64')
            }));
        });
    });

    ws.endpoint.listen(port);
    ws.endpoint.port = port;
    // tell client what port he has
    console.log(`Welcome client ${ws.id} on port ${port}`);
    ws.send(JSON.stringify({ $type: 'Welcome', port }));
    // broadcast to matchmaking

    ws.on('message', (data) => {
        const message = JSON.parse(data);

        if (message.$type === 'Packet') {
            const connection = message.connection;
            if (message.payload &&
                message.payload.$type &&
                message.payload.$type === 'Byte[]' &&
                message.payload.$value) {
                for (const player of ws.players) {
                    if (player.id === connection) {
                        console.log('Sending message to client');
                        player.write(Buffer.from(message.payload.$value, 'base64'));
                        break;
                    }
                    console.log('No connection found for this packet');
                }
            } else {
                console.log('Bad type - not forwarding this message');
            }
        } else if (message.$type === 'Close') {
            console.log(`Client sent ${data}`);
            const connection = message.connection;
            for (const player of ws.players) {
                if (player.id === connection) {
                    console.log('Client asked to disconnect client');
                    player.destroy();
                    break;
                }
                console.log('Found no player to disconnect');
            }
        }
    });

    ws.once('close', () => {
        console.log(`Client ${ws.id} closed connection`);
        for (const player of ws.players) {
            console.log(`Disconnecting player ${player.id} of ${ws.id}`);
            player.destroy();
        }
        ws.players.clear();
        console.log(`Stopping endpoint for ${ws.id}`);
        ws.endpoint.close(() => {
            console.log(`Returned port ${ws.endpoint.port} of ${ws.id}`);
            ports.push(ws.endpoint.port);
        });
    });
});

const dgram = require('dgram');
const socket = dgram.createSocket('udp4');
const template = Buffer.from(
    '0000809367616d655f6d6f64653d333b6d6178706c3d343b6e6174696f6e3d656e3b6e756d706c3d313b70617373776f72643d303b706c61796572733da72831' +
    '30291b6e6f426c7562621ca7282d31291b3b7365727665725f6e616d653d6e6f426c75626220286e6f426c756262293b7365727665725f706f72743d35303030' +
    '363b766d616a3d313b766d696e3d333b76747970653d66',
    'hex'
);

socket.on('message', (msg, info) => {
    console.log(msg.toString('hex'), msg.toString(), info);
    const id = msg.readUInt16LE(0);
    template.writeUInt16LE(id, 0);
    socket.send(template, info.port, info.address);
});
socket.bind(12345);
