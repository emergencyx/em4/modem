const net = require('net');
const WebSocket = require('ws');

function setImmediatePromise() {
    return new Promise((resolve) => {
        setImmediate(() => resolve());
    });
}


let players = [];
const ws = new WebSocket('ws://mars.emergencyx.de:40000');
ws.on('open', () => {
    console.log('Websocket open');
});
ws.on('message', async (data) => {
    const message = JSON.parse(data);
    if (message.$type === 'PlayerConnected') {
        const player = net.createConnection(58282, '127.0.0.1', () => {
            console.log(`Connection {} established`, player.id);
            player.on('data', async (incoming) => {
                player.pause();
                ws.send(JSON.stringify({
                    $type: 'Packet',
                    payload: { $type: 'Byte[]', $value: incoming.toString('base64') },
                    connection: player.id
                }));
                await setImmediatePromise();
                player.resume();
            });
            player.once('close', () => {
                console.log(`Player ${player.id} disconnected`);
                ws.send(JSON.stringify({
                    $type: 'Close',
                    connection: player.id
                }));
                players = players.filter(p => p !== player);
            });
        });
        player.id = message.connection;
        players.push(player);
        console.log(`New incoming connection ${player.id}`);
    } else if (message.$type === 'Packet') {
        await setImmediatePromise();
        for (const player of players) {
            if (player.id === message.connection) {
                player.write(Buffer.from(message.payload, 'base64'));
                return;
            }
        }
        console.log(`Unable to forward packet for ${message.connection}`);
    } else if (message.$type === 'PlayerDisconnected') {
        for (const player of players) {
            if (player.id === message.connection) {
                player.destroy();
                return;
            }
        }
        console.log(`Unable to disconnected player for ${message.connectio}`);
    } else {
        console.log(message);
    }
});
